﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.IO;

namespace GDIKsam2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int count = 1;
            richTextBox1.Clear();
            string[] textMass, searchMass;
            int countCoincidence;
            double relevance, relevance2;
            string s0 = textBox1.Text;
            string s0buf = s0;
            string s, sbuf;
            string[] files = new DirectoryInfo(@".\").GetFiles("*.txt", SearchOption.AllDirectories)
          .Select(f => f.FullName).ToArray();
            if (s0.Split(' ').Length <= 5 && textBox1.Text != "")
            {
                foreach (string element in files)
                {

                    using (StreamReader sr = new StreamReader(element))
                    {
                        s = sr.ReadToEnd();
                    }
                    sbuf = s;
                    searchMass = s0.Split(' ');
                    textMass = sbuf.Split(' ');
                    relevance = (double)CountWords(s, s0) / (textMass.Length - searchMass.Length + 1);
                    richTextBox1.AppendText("Релевантность совпадений для фразы целиком и меньше в файле "
                        + Path.GetFileName(element) + "\n " + relevance.ToString("0.00", CultureInfo.InvariantCulture));
                    while (s0buf.IndexOf(' ') > 0 && (textMass.Length - searchMass.Length + 1) > 0)
                    {
                        s0buf = s0buf.Remove(s0buf.LastIndexOf(' '));
                        searchMass = s0buf.Split(' ');
                        countCoincidence = CountWords(s, s0buf);
                        relevance2 = (double)countCoincidence/(textMass.Length - searchMass.Length + 1);
                        richTextBox1.AppendText("\n" + relevance2.ToString("0.00", CultureInfo.InvariantCulture) + " ");
                    }
                    richTextBox1.AppendText("\n");
                    count++;
                    s0buf = s0;
                    sbuf = s;
                }
            }
            else if (textBox1.Text == "" || textBox1.Text == " ")
            {
                MessageBox.Show("Введите запрос");
            }
            else
            {
                MessageBox.Show("Введите меньше 6 слов");
            }
        }

        private int CountWords(string s, string s0)
        {
            int count = (s.Length - s.Replace(s0, "").Length) / s0.Length;
            return count;
        }
    }
}
